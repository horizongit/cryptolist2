import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
require('dotenv').config()
//https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=10&page=1&sparkline=fals
Vue.use(Vuex);
Vue.use(VueAxios);
export default new Vuex.Store({
  state: {
    todos: [],
  },
  getters: {
    allTodos: (state) => state.todos,
  },
  mutations: {},
  actions: {
    loadItems() {
      axios
        .get(`${process.env.VUE_APP_URL}`, {})
        .then((response) => response.data)
        .then((todos) => {
          this.state.todos = todos;
        });
    },
  },
  modules: {},
});
